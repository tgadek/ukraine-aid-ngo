<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid NGO</title>
    <link rel = "icon" href = "donation_symbol.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    p {text-align: right;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0; max-width: 820px;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal; max-width: 820px;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal; max-width: 820px;}
    .tg .tg-vxqb1{ background-color: SeaGreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: Green; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb3{ background-color: white; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:left;vertical-align:center}


    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    
    img { max-width: 800px; height: auto;}
    .img2 { max-width: 800px; height: auto;}
    .pink_bold {color: pink; font-weight: bold;} 
    .lightblue_bold {color: lightblue; font-weight: bold;} 

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="hosts.php">SUPPORTED HOSTS + OFFERS</a></th> 
                  <th class="tg-vxqb1"><a href="status.php">STATUTES</a></th>                                 
              </tr>
              <tr>    
                  <th class="tg-vxqb2"><a href="team.php">UKRAINE AID TEAM</a></th>                     
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS + PARTNERS</a></th>  
                  <th class="tg-vxqb2"><a href="donate.php">DONATE</a></th>                                    
              </tr>
            </thead>
        </table>
        
        </h1>
        <div align ="center">
        Temporarily you can donate to 3 private bank accounts in CHF, EUR and PLN, the association's bank accounts will be opened and updated here soon. 
        <br /><br />Please write in the transfer title "Donation for Ukraine Aid". 
        <br /><br />Thank you for your good heart<span style="color:red">&#10084;</span>!
        <br />
        <br />
        <br />&#9734; <b>ACCOUNT IN CHF</b> &#9734;
        <br /><br />
        <table class="tg" width="80%" align="center">
            <tbody>
                <tr>              
                  <td class="tg-vxqb3">Account holder:</td>
                  <td class="tg-vxqb3">Tomasz Gadek</td>
                </tr>
                <tr>              
                  <td class="tg-vxqb3">IBAN:</td>
                  <td class="tg-vxqb3">CH330027927913960640A</td>
                </tr>
                <tr>              
                  <td class="tg-vxqb3">BIC:</td>
                  <td class="tg-vxqb3">UBSWCHZH80A</td>
                </tr>                         
            </tbody>
        </table>       
        
        <br />
        <br />&#9734; <b>ACCOUNT IN EUR</b> &#9734;
        <br /><br />
        <table class="tg" width="80%" align="center">
            <tbody>
                <tr>              
                  <td class="tg-vxqb3">Account holder:</td>
                  <td class="tg-vxqb3">Tomasz Gadek</td>
                </tr>
                <tr>              
                  <td class="tg-vxqb3">IBAN:</td>
                  <td class="tg-vxqb3">BE48967018222727</td>
                </tr>
                <tr>              
                  <td class="tg-vxqb3">BIC:</td>
                  <td class="tg-vxqb3">TRWIBEB1XXX</td>
                </tr>                         
            </tbody>     
        </table>
        <br />
        <br />&#9734; <b>ACCOUNT IN PLN (only inside Poland)</b> &#9734;
        <br /><br />
        <table class="tg" width="80%" align="center">
        <tbody>
                <tr>              
                  <td class="tg-vxqb3">Account holder:</td>
                  <td class="tg-vxqb3">Tomasz Gadek</td>
                </tr>
                <tr>              
                  <td class="tg-vxqb3">Account number: </td>
                  <td class="tg-vxqb3">89102010263741000000029823</td>
                </tr>                                    
            </tbody>
        </table>
        <br />
        <br />
      <img position="absolute" src="donation_symbol.png" alt="Donation_symbol" align="center" width="100px">
    </div>
</body>
<footer width="60%" align = "right">
    <foot width="60%"> 
    
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145358" alt="Hits counter" style="border:1px;" /></p>
    
    </foot>
</footer>
</html>
