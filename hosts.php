<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid NGO</title>
    <link rel = "icon" href = "donation_symbol.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    p {text-align: right;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0; max-width: 810px;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal; max-width: 810px;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal; max-width: 810px;}
    .tg .tg-vxqb1{ background-color: SeaGreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: Green; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb3{ background-color: white; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:left;vertical-align:center}
    .tg .tg-vxqb4{ background-color: ForestGreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:left;vertical-align:center}


    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    
    img { max-width: 800px; height: auto;}
    .img2 { max-width: 800px; height: auto;}
    .pink_bold {color: pink; font-weight: bold;} 
    .lightblue_bold {color: lightblue; font-weight: bold;} 

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="hosts.php">SUPPORTED HOSTS + OFFERS</a></th> 
                  <th class="tg-vxqb1"><a href="status.php">STATUTES</a></th>                                 
              </tr>
              <tr>    
                  <th class="tg-vxqb2"><a href="team.php">UKRAINE AID TEAM</a></th>                      
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS + PARTNERS</a></th>   
                  <th class="tg-vxqb2"><a href="donate.php">DONATE</a></th>                                    
              </tr>
            </thead>
        </table>        
        </h1>

        <div align ="center">
        <br />&#9734; <b>OUR OFFER OF SUPPORTING HOSTS</b> &#9734;
        <br />
        <br />Financial support given to hosts is proportional to the economical situation of their country, average monthly household expenses and groceries basket price. Household expenses are calculated as the overhead of the hosts expenses w.r.t. the bills from the same month of the previous year's expenses (just the change of the bill prices is taken into account). Minimum requirement for a host to receive support is that at least one child is being hosted. Our mission is to provide the best possible environment for a fresh start of young population and give an opportunity for a stress-less education. The monthly support is provided for a period of 12 months, after which we assume that the hosted families assimilate well in their host country, e.g., learn the language and find other sources of income. The hosts are free to share this support directly with hosted families, up to 100% of transferred funds.
<br />
<br />
<table class="tg1" width="100%" align="center">
            <thead>
                <tr>    
                  <th >COUNTRY</th>                  
                  <th >SUBSISTANCE</th> 
                  <th >SUPPLEMENT/person</th>  
                  <th >PERIOD</th>      
                </tr>
                <tr>    
                  <th >Poland</th>                  
                  <th >500 PLN</th> 
                  <th >+100 PLN</th>  
                  <th >12 months</th>      
                </tr>
                <tr>    
                  <th >Switzerland</th>                  
                  <th >200 CHF</th> 
                  <th >+75 CHF</th>  
                  <th >12 months</th>        
                </tr>
                <tr>    
                  <th >France</th>                  
                  <th >150 EUR</th> 
                  <th >+50 EUR</th>  
                  <th >12 months</th>     
                </tr>
                <tr>    
                  <th >Italy</th>                  
                  <th >150 EUR</th> 
                  <th >+50 EUR</th>  
                  <th >12 months</th>        
                </tr>
              
            </thead>
        </table>
 
        

<br />
        <br />&#9734; <b>OUR WAY OF SUPPORTING HOSTS</b> &#9734;
        <br />
        <br />We are performing a background check of every host who sends a request for funding. <b>Hosted refugees must be registered to the local authorities</b>. From those accepted, we require a monthly report on how the funds were spent, together with photo proofs. For example, a receipt from a shop, where: the date, list of purchased products and the total amount paid are all clearly visible accompanied by a photo of purchased products. For bills payments we require attestations of bank transfers, with: the date and the account holder as well as the beneficiary. Due to a large number of requests our processing time can take up to 2 weeks before we come to you with an answer - please do not re-send requests. Rejected requests will not be processed again, we are really sorry for that.

<br />
<br />&#9734; <b>CURRENTLY SUPPORTED HOSTS</b> &#9734;
<br /><br />
</div>

<table class="tg" width="60%" align="center">
  <tbody>
  <tr>              
    <td class="tg-vxqb1"><img position="absolute" src="people/jandp.jpg" alt="JandP"  align="center" width="300px"></td>                  
    <td class="tg-vxqb2">
      Jolanta&nbsp;R.
      <br /> and 
      <br /> Patryk&nbsp;R.</td>  
    <td class="tg-vxqb1">
      <br /> Gdynia, Poland
      <br /> contact: zulv [at] op.pl
      <br />
      <br /> Hosting 5 people (2 adults with 3 children):
      <br /> Tetiana S. with 1 child: Anhelina&nbsp;(6y.o.) 
      <br /> Lyudmyla M. with 2 children: Matviy&nbsp;(1y.o.) and Rostislav&nbsp;(11y.o.)
    </td>
  </tr>  
  <tr>              
    <td colspan="3" class="tg-vxqb4">
      <img position="absolute" width = "810px" class="img2" src="people/family1.jpg" alt="family1" align="center" >
    </td>                  
  </tr>
  </tbody>
</table>

<br />
<br />

<table class="tg" width="60%" align="center">
  <tbody>
  <tr>              
    <td class="tg-vxqb1">
      <img position="absolute" src="people/eandc.jpeg" alt="EandC"  align="center" width="300px">    
    </td>                  
    <td class="tg-vxqb2">
      Elisabeth&nbsp;H.
      <br /> and 
      <br /> Christian&nbsp;H.</td>  
    <td class="tg-vxqb1">
      <br /> Bellegarde, France
      <br /> Tel: +33 651 one two 32 01      
      <br /> 
      <br /> Hosting 3 people (2 adults with 1 child):
      <br /> Larissa H. and Igor H. with 1 child: Diana&nbsp;(5y.o.)
    </td>
  </tr>
  <tr>              
    <td colspan="3" class="tg-vxqb4">
      <img position="absolute" width = "810px" class="img2" src="people/family2.jpg" alt="family2" align="center">    
    </td>                  
  </tr>
  </tbody>
</table>

</body>

<footer width="60%" align = "right">
    <foot width="60%"> 
    
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145358" alt="Hits counter" style="border:1px;" /></p>
    
    </foot>
</footer>
</html>
