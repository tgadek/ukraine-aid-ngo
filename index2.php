<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ukraine Aid NGO</title>
    <link rel = "icon" href = "donation_symbol.png" type = "image/x-icon">
    <style type="text/css">
    h1 {text-align: center; font-family: Helvetica, sans-serif;}
    p {text-align: right;}
    A {text-decoration: none; color: white; font-weight: bold;}
    a {text-decoration: none; color: white; font-weight: bold;}
    div {text-align: center; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    .div2 {text-align: right; align: center; font-family: Helvetica, sans-serif; font-size: 18px; width: 60%; align-items: center; justify-content: center; margin: auto; max-width: 820px;}
    info {text-align: center; font-family: Helvetica, sans-serif; font-size: 15px;}
    foot {text-align: right; font-family:"Helvetica", Helvetica, sans-serif; font-size:10px;}
    .tg  {border-collapse:collapse;border-spacing:0; max-width: 820px;}
    .tg td{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:18px;
      overflow:hidden;padding:5px 5px;word-break:normal; max-width: 820px;}
    .tg th{border-color:black;border-style:solid;border-width:0px;font-family:Helvetica, sans-serif;font-size:24px;
      font-weight:normal;overflow:hidden;padding:5px 5px;word-break:normal; max-width: 820px;}
    .tg .tg-vxqb1{ background-color: SeaGreen; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb2{ background-color: Green; color: white; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:center;vertical-align:center}
    .tg .tg-vxqb3{ background-color: white; color: black; font-family:"Helvetica", Helvetica, sans-serif !important;text-align:left;vertical-align:center}


    .tg1 td{border: 3px solid black;}
    .tg1 th{border: 3px solid black;}
    .tg1 {border-collapse: collapse; border-style: hidden;}

    
    img { max-width: 800px; height: auto;}
    .img2 { max-width: 800px; height: auto;}
    .green_bold {color: green; font-weight: bold;} 
    .pink_bold {color: pink; font-weight: bold;} 
    .lightblue_bold {color: lightblue; font-weight: bold;} 

    </style>
</head>
<body>

    <h1>
        <table class="tg" width="60%" align="center">
            <thead>
              <tr>                  
                  <th class="tg-vxqb1"><a href="index.php">HOMEPAGE</a></th>
                  <th class="tg-vxqb2"><a href="hosts.php">SUPPORTED HOSTS + OFFERS</a></th> 
                  <th class="tg-vxqb1"><a href="status.php">STATUTES</a></th>                                 
              </tr>
              <tr>    
                  <th class="tg-vxqb2"><a href="team.php">UKRAINE AID TEAM</a></th>                  
                  <th class="tg-vxqb1"><a href="sponsors.php">SPONSORS + PARTNERS</a></th>   
                  <th class="tg-vxqb2"><a href="donate.php">DONATE</a></th>                                    
              </tr>
            </thead>
        </table>
        
        </h1>
        <div align ="center">
        <br />&#9734; <b>MISSION</b> &#9734;
        <br />
        <br />Hello! <a href="http://ukraineaidngo.app.cern.ch/"><span class="green_bold">Ukraine Aid</span></a> is a young non-governmental association registered in Geneva, Switzerland. It has been established out of a clear need for a quick response to the recent emergency situation in eastern Europe. Our main goal is to provide financial support to families relocated abroad due to military conflicts in their home countries. Our intention is to help creating a safe, welcoming environment and increase chances for a fresh start in a host country. The relocated families do not need to have an official refugee status, but must be registered at the local authorities and possess a proof of border-crossing after the outbreak of the conflict. For the moment we are focusing on the situation of Ukrainian citizens fleeing the war and being hosted in EU countries. Our fixed costs and bureaucracy involved are next to zero. We managed to limit the operational costs by running the association entirely by ourselves as volunteers. This is to ensure that 100% of <span class="lightblue_bold">YOUR</span> donations end up with those who <span class="lightblue_bold">YOU</span> choose to support.
        <br />
        <br /> Our target is to support at least 10 families in the second half of 2022. We are very committed and will do our best to achieve this goal.
        
        </div>
        <div class="div2">
        <br />- Tomasz Gadek<br /> Ukraine Aid founder
        </div>
        
        <div align ="center">
        <br />
        <br />&#9734; <b>CHARITY CONCERTS</b> &#9734;
        <br />
        <br />
        We are organizing a series of <a href="http://ukraineaid.app.cern.ch"><span class="pink_bold">charity concerts</span></a> to raise funds for  humanitarian aid in Ukraine via our associations. We have observed a drastic loss of interest in donations for the issue that is not resolved yet. We would like to raise awareness and offer <span class="lightblue_bold">YOU</span> a bit of entertainment in exchange for <span class="lightblue_bold">YOUR</span> financial support. We cordially invite you to participate in <a href="http://ukraineaid.app.cern.ch"><span class="pink_bold">our concerts</span></a>.
        <br />
        <br />
        <a href="http://ukraineaid.app.cern.ch"><img position="absolute" src="logo_concert.png" alt="Donation_symbol" align="center" width="600px"></a>
        <br />
        </div>    
</body>
<footer width="60%" align = "right">
    <foot width="60%"> 
    
    <br /> powered by Tomasz Gadek & coffee <br />
    <p><img src="https://www.darmowylicznik.pl/licznik.php?id=145358" alt="Hits counter" style="border:1px;" /></p>
    
    </foot>
</footer>
</html>
